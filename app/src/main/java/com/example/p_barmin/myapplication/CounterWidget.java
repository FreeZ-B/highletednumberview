package com.example.p_barmin.myapplication;

/**
 * Created by p_barmin on 09.04.15.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class CounterWidget extends View {

    private final String TAG = "CounterWidget";

    private final String SEPARATOR = ",";
    private final int SEPARATOR_COUNT = 3;
    private final float SEPARATOR_SIZE_RATIO = 2.2f;

    private final int DEFAULT_TEXT_COLOR = Color.WHITE;
    private final int DEFAULT_RECT_COLOR = Color.parseColor("#cbcbcb");
    private final int DEFAULT_TEXT_SIZE = 35;

    private final float RECT_PADDING_HORIZONTAL_RATIO = 0.3f;
    private final float RECT_PADDING_VERTICAL_RATIO = 0.4f;

    private final int RECT_PADDING_BETWEEN = 10;

    private int mRectColor;
    private int mTextColor;
    private int mTextSize;

    private int mRectHorizontalPadding;
    private int mRectVerticalPadding;

    private Paint mRectPaint;
    private Paint mNumberPaint;
    private Paint mCommaPaint;

    private int mRectWidth;
    private int mRectHeight;
    private int mCommaWidth;

    private String mFullNumber;
    private final int mNumber;

    public CounterWidget(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CounterWidget, 0, 0);
        try {
            mRectColor = a.getColor(R.styleable.CounterWidget_rectColor, DEFAULT_RECT_COLOR);
            mTextColor = a.getColor(R.styleable.CounterWidget_textColor, DEFAULT_TEXT_COLOR);
            mTextSize = a.getDimensionPixelOffset(R.styleable.CounterWidget_textSize, DEFAULT_TEXT_SIZE);
            mNumber = a.getInteger(R.styleable.CounterWidget_textNumber, 1000000000);
            mFullNumber = String.valueOf(mNumber);
        } finally {
            a.recycle();
        }

        init();
    }

    private void init() {
        initPaints();

        mFullNumber = separateNumber(mNumber);

        // Adding additional padding
        int[] digitMaxSize = getDigitMaxSize();

        mRectHorizontalPadding = (int) (digitMaxSize[0] * RECT_PADDING_HORIZONTAL_RATIO);
        mRectVerticalPadding = (int) (digitMaxSize[1] * RECT_PADDING_VERTICAL_RATIO);

        mRectWidth = digitMaxSize[0] + mRectHorizontalPadding * 2;
        mRectHeight = digitMaxSize[1] + mRectVerticalPadding * 2;

        mCommaWidth = (int) mCommaPaint.measureText(SEPARATOR);
    }

    private void initPaints(){
        // Paint object for the rectangles.
        mRectPaint =new Paint(Paint.ANTI_ALIAS_FLAG);
        mRectPaint.setStyle(Paint.Style.FILL);
        mRectPaint.setStrokeCap(Paint.Cap.ROUND);
        mRectPaint.setColor(mRectColor);

        // Paint object for the number.
        mNumberPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        mNumberPaint.setStyle(Paint.Style.FILL);
        mNumberPaint.setStrokeCap(Paint.Cap.ROUND);
        mNumberPaint.setColor(mTextColor);
        mNumberPaint.setTextSize(mTextSize);

        // Paint object for the comma.
        mCommaPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        mCommaPaint.setStyle(Paint.Style.FILL);
        mCommaPaint.setStrokeCap(Paint.Cap.ROUND);
        mCommaPaint.setColor(mRectColor);
        mCommaPaint.setTextSize(mTextSize * SEPARATOR_SIZE_RATIO);
    }

    private int[] getDigitMaxSize(){
        return new int[]{getDigitMaxWidth(mNumberPaint), mTextSize};
    }

    private int getDigitMaxWidth(Paint paint){
        int maxWidth = 0;
        for (int i = 0; i < 10; i++){
            if (paint.measureText(String.valueOf(i)) > maxWidth){
                maxWidth = (int) paint.measureText(String.valueOf(i));
            }
        }

        return maxWidth;
    }

    /**
     * Separates number with commas (or other separator)
     * @param n Complete number (f.e 120000)
     * @return separated string
     */
    private String separateNumber(int n) {
        int number = n;
        String stringNumber = String.valueOf(n);
        int comaIndex = stringNumber.length() - SEPARATOR_COUNT;

        StringBuilder builder = new StringBuilder();
        while ((number /= Math.pow(10, SEPARATOR_COUNT)) >= 1) {
            builder.insert(0, SEPARATOR + stringNumber.substring(comaIndex, comaIndex + SEPARATOR_COUNT));
            comaIndex -= SEPARATOR_COUNT;
        }

        return builder.insert(0,
                stringNumber.substring(0, comaIndex + SEPARATOR_COUNT)).toString();
    }

    private int calculateMaxAvailableTextSize(final int width){
        int separatorQuantity = 0;
        int digitQuantity = 0;
        boolean isLastDigit = true;
        // Getting separator and digits quantities
        for (char c : mFullNumber.toCharArray()){
            if (Character.isDigit(c)){
                if (!isLastDigit){
                    separatorQuantity++;
                }
                digitQuantity++;
                isLastDigit = true;

            } else {
                isLastDigit = false;
            }
        }

        int maxWidth = (int) (width / (digitQuantity + digitQuantity * 2 * RECT_PADDING_HORIZONTAL_RATIO +  separatorQuantity * SEPARATOR_SIZE_RATIO));

        int currWidth= getDigitMaxSize()[0];
        int size = mTextSize;

        Paint tmpPaint = new Paint();
        tmpPaint.setTextSize(size);
        // Trying to find most similar font size
        while (Math.abs(maxWidth - currWidth) > 4f){
            currWidth = getDigitMaxWidth(tmpPaint);
            if (maxWidth > currWidth){
                size += size / 2;
                tmpPaint.setTextSize(size);
            } else if (maxWidth < currWidth){
                size -= size / 2;
                tmpPaint.setTextSize(size);
            } else {
                break;
            }
        }

        return size;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        // Do not allow overdrawing bounds!
        int maxTextSize = calculateMaxAvailableTextSize(right - left);
        if (mTextSize > maxTextSize || getLayoutParams().width == ViewGroup.LayoutParams.MATCH_PARENT){
            mTextSize = maxTextSize;
            // Don't forget to update all margins, paddings and so on
            init();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Clear everything

        int start = 0;
        for (int i = 0; i < mFullNumber.length(); i++) {
            if (String.valueOf(mFullNumber.charAt(i)).equals(SEPARATOR)){
                // Separator
                canvas.drawText(String.valueOf(mFullNumber.charAt(i)), start, mRectHeight, mCommaPaint);

                start += mCommaWidth;
            } else {
                if (i != 0){
                    start += RECT_PADDING_BETWEEN;
                }

                Rect digitBounds = new Rect(start, 0, start + mRectWidth, mRectHeight);
                canvas.drawRect(digitBounds, mRectPaint);

                int textX = digitBounds.left + mRectHorizontalPadding;
                int textY = digitBounds.bottom - mRectVerticalPadding;
                canvas.drawText(String.valueOf(mFullNumber.charAt(i)), textX, textY, mNumberPaint);

                start += digitBounds.width();
            }

        }
    }

}